import retro
import configparser
import numpy as np
import imageio
import pickle
import sys 
import os

config = configparser.ConfigParser()
config.read('rm_config.conf')
retro_name = config['DEFAULT']['RetroName']
experiment_name = config['DEFAULT']['ExperimentName']
console_name = config['DEFAULT']['ConsoleName']

game_state_frequency = int(config['BK2']['GamestateSaveRate'])
screencap_frequency = int(config['BK2']['ScreencapSaveRate'])
ramcap_frequency = int(config['BK2']['RAMSaveRate'])
RAM_start = int(config['DEFAULT']['RAMStart'])
RAM_end = int(config['DEFAULT']['RAMend'])

# movie = retro.Movie(retro_name + "/" experiment_name + retro_name + "_recording.bk2")

if not sys.argv[1] or (".bk2" not in sys.argv[1]):
    print("No valid movie selected, exiting")
    exit()

print(sys.argv[1])
movie = retro.Movie(sys.argv[1])
movie.step()

env = retro.make(
    game=movie.get_game(),
    state=None,
    # bk2s can contain any button presses, so allow everything
    use_restricted_actions=retro.Actions.ALL,
    players=movie.players,
)
env.initial_state = movie.get_state()
env.reset()

steps=0

# Update the result dir path for BK2 Analysis
result_dir_path = f"GameplayData/{retro_name}/{experiment_name}/BK2Analysis"

while movie.step():
    keys = []
    # Get all of the keys at this frame and load it into the array. This is the button press combination that we will feed into the emulator
    for p in range(movie.players):
        for i in range(env.num_buttons):
            keys.append(movie.get_key(i, p))
    steps+=1
    # print(f"step: {steps}")

    if(steps % screencap_frequency == 0):
        # Write out a screencap, as well as a bin file of the buttons pressed here
        # This also guarantees that the button presses here matches the screenshot taken
        imageio.imwrite(os.path.join(result_dir_path + "/Screenshots", f"BK2_Screencap_{steps}.png"), env.render(mode='rgb_array'))
        with open(os.path.join(result_dir_path + "/Buttons", f"BK2_buttons_{steps}.bin"), 'wb') as f:
            # np.save(f, keys)
            np.array(keys).tofile(f)

    if(steps % ramcap_frequency == 0):
        ramcap = env.get_ram()[RAM_start:RAM_end]
        # print("SNES RAM snapped: " + str(snap_counter))
        rams_temp = np.array(ramcap)
        with open(os.path.join(result_dir_path + "/RAM", f"BK2_ramcap_{steps}.bin"), 'wb') as f:
            # np.save(f, rams_temp)
            rams_temp.tofile(f)

    if(steps % game_state_frequency == 0):
        # imageio.imwrite(os.path.join(result_dir_path, filename), self._env.get_ram()[RAM_start:RAM_end])
        state = env.em.get_state()
        state_path = os.path.join(result_dir_path + "/States", f"BK2_gameplay_{steps}.state")
        with open(state_path, 'wb') as f:
            pickle.dump(state, f,  pickle.HIGHEST_PROTOCOL)
    env.step(keys)
