import gym
import pygame
import sys
import time
import matplotlib
import numpy as np
try:
    matplotlib.use('GTK3Agg')
    import matplotlib.pyplot as plt
except Exception:
    pass

import pyglet.window as pw

from collections import deque
from pygame.locals import HWSURFACE, DOUBLEBUF, RESIZABLE, VIDEORESIZE
import pickle
from threading import Thread

# KeyboardCodes.UP_ARROW    : RetroSnesButtonMaps.UP,
# KeyboardCodes.DOWN_ARROW  : RetroSnesButtonMaps.DOWN,
# KeyboardCodes.LEFT_ARROW  : RetroSnesButtonMaps.LEFT,
# KeyboardCodes.RIGHT_ARROW : RetroSnesButtonMaps.RIGHT,

# KeyboardCodes.SPACE   :  RetroSnesButtonMaps.A,
# KeyboardCodes.L_SHIFT :  RetroSnesButtonMaps.Y,
# KeyboardCodes.S       :  RetroSnesButtonMaps.B,
# KeyboardCodes.D       :  RetroSnesButtonMaps.X,

# KeyboardCodes.O : RetroSnesButtonMaps.SELECT,
# KeyboardCodes.P : RetroSnesButtonMaps.START,

# KeyboardCodes.W : RetroSnesButtonMaps.L,
# KeyboardCodes.E : RetroSnesButtonMaps.R

def display_arr(screen, arr, video_size, transpose):
    arr_min, arr_max = arr.min(), arr.max()
    arr = 255.0 * (arr - arr_min) / (arr_max - arr_min)
    pyg_img = pygame.surfarray.make_surface(arr.swapaxes(0, 1) if transpose else arr)
    pyg_img = pygame.transform.scale(pyg_img, video_size)
    screen.blit(pyg_img, (0,0))

def play(env, transpose=True, fps=60, zoom=None, callback=None, keys_to_action=None):

    obs_s = env.observation_space
    assert type(obs_s) == gym.spaces.box.Box
    assert len(obs_s.shape) == 2 or (len(obs_s.shape) == 3 and obs_s.shape[2] in [1,3])
    
    if "Snes" in env.gamename:
        with open("retro_engine/KeyMappings/SNES_button_mapping.pickle", "rb") as f:
            keys_to_action = pickle.load(f)
    elif "GameBoy" in env.gamename:
        with open("retro_engine/KeyMappings/GB_button_mapping.pickle", "rb") as f:
            keys_to_action = pickle.load(f)
    else:
        print('\033[91m' + "Error: The key mapping for the current platform isn't there. Go to integration folder and run the script in the Key_Mapping folder." +  '\033[0m')
        exit()
    relevant_keys = set(sum(map(list, keys_to_action.keys()),[]))
    print(len(keys_to_action))
    video_size = env.observation_space.shape[1], env.observation_space.shape[0]
    video_size = int(video_size[0] * zoom), int(video_size[1] * zoom)

    pressed_keys = []
    running = True
    env_done = True

    screen = pygame.display.set_mode(video_size)
    clock = pygame.time.Clock()

    while running:
        if env_done:
            env_done = False
            obs = env.reset()
        else:
            action = keys_to_action.get(tuple(sorted(pressed_keys)), 0)
            prev_obs = obs
            obs, rew, env_done, info = env.step(action)
            yield obs, info
            if callback is not None:
                callback(prev_obs, obs, action, rew, env_done, info)
        if obs is not None:
            if len(obs.shape) == 2:
                obs = obs[:, :, None]
            if obs.shape[2] == 1:
                obs = obs.repeat(3, axis=2)
            display_arr(screen, obs, transpose=transpose, video_size=video_size)

        # process pygame events
        for event in pygame.event.get():
            # test events, set key states
            if event.type == pygame.KEYDOWN:
                if event.key in relevant_keys:
                    pressed_keys.append(event.key)
                elif event.key == 27:
                    running = False
            elif event.type == pygame.KEYUP:
                if event.key in relevant_keys:
                    pressed_keys.remove(event.key)
            elif event.type == pygame.QUIT:
                running = False
            elif event.type == VIDEORESIZE:
                video_size = event.size
                screen = pygame.display.set_mode(video_size)

        pygame.display.flip()
        clock.tick(fps)
    pygame.quit() 
