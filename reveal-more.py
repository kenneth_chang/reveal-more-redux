# Make it so that this is not imported in MCTS
# from keras.models import load_model
import pickle, glob
import argparse
import shutil
import os, sys, six
import numpy as np
import imageio
import time
import random
import configparser
import retro

# Remove later
TILE_SIZE = 32

# Load in config data
config = configparser.ConfigParser()
config.read('rm_config.conf')
experiment_name = config['DEFAULT']['ExperimentName']
console_name = config['DEFAULT']['ConsoleName']
retro_name = config['DEFAULT']['RetroName']
save_keypresses = config['DEFAULT']['SaveKeypresses']
RAM_start = int(config['DEFAULT']['RAMStart'])
RAM_end = int(config['DEFAULT']['RAMend'])

revisit_count = int(config['RM']['RevisitCount'])
dwell_time = int(config['RM']['DwellForFrames'])
game_state_frequency = int(config['RM']['GamestateSaveRate'])
screencap_frequency = int(config['RM']['ScreencapSaveRate'])
ramcap_frequency = int(config['RM']['RAMcapSaveRate'])
algorithm = config['RM']['Algorithm']
norender = config['RM']['Rendering']

def get_image(self, _obs, env):
    return env.render(mode='rgb_array')

def get_ram(self, _obs, env, start, end):
    return env.get_ram()[start:end]

def random_play(env, states:np.ndarray, ex_dir_path, dist=False):
    ex_dir_image_path = ex_dir_path + f"/RMGameplay/Screenshots"
    ex_dir_ram_path = ex_dir_path + f"/RMGameplay/RAM"
    ex_dir_state_path = ex_dir_path + f"/RMGameplay/States"
    ex_dir_dist_path = ex_dir_path + f"/KeyDistrbutions"
    start_time = time.time()

    #If we are using weighted random buttin pressing
    if dist:
        print("RM Random Alg is using a weighted button distribution")
        distribution = np.load(ex_dir_dist_path)
    else:
        print("RM Random Alg is NOT using a weighted button distribution")

    def get_action():
        if dist:
            temp_action = np.random.choice(4096, p=distribution)
            # Convert the action int to a binary representation of that number
            str_action = f'{temp_action:b}'.rjust(12, '0')[::-1]

            # Make the list int
            action = [int(str_button) for str_button in str_action]
        else:
            action = env.action_space.sample()
        return action

    #restart the env, prepare to start loading
    obs = env.reset()

    print("Running RM on " + str(len(states)) + " states, each state will be played for " + str(dwell_time) + " frames")
    steps = 0                           # How many frames were stepped through.

    for idx, start_point in enumerate(states):
        print(idx)
        env.em.set_state(start_point)
        # t_end = time.time() + state_run_length # Allow each state to run for given time / num of states
        in_state_frames = 0
        while in_state_frames < dwell_time: # In frames
            chosen_action = get_action()

            obs, _, _, _info = env.step(chosen_action)

# ex_dir_image_path = ex_dir_path + f"/RMGameplay/Screenshots"
# ex_dir_ram_path = ex_dir_path + f"/RMGameplay/RAM"
# ex_dir_state_path = ex_dir_path + f"/RMGameplay/States"
# ex_dir_dist_path = ex_dir_path + f"/KeyDistrbutions"

# revisit_count = config['RM']['RevisitCount']
# dwell_time = config['RM']['DwellForFrames']
# game_state_frequency = config['RM']['GamestateSaveRate']
# screencap_frequency = config['RM']['ScreencapSaveRate']
# ramcap_frequency = config['RM']['RAMcapSaveRate']

            if(in_state_frames % screencap_frequency == 0):
                imageio.imwrite(os.path.join(ex_dir_image_path, f"RM_Screencap_{steps}.png"), env.render(mode='rgb_array'))

            if(in_state_frames % ramcap_frequency == 0):
                ramcap = env.get_ram()[RAM_start:RAM_end]
                # print("SNES RAM snapped: " + str(snap_counter))
                rams_temp = np.array(ramcap).tobytes()
                ram_file_handle = open(os.path.join(ex_dir_ram_path, f"RM_ramcap_{steps}.bin"), 'wb')
                np.save(ram_file_handle, rams_temp)

            if(in_state_frames % game_state_frequency == 0):
                # imageio.imwrite(os.path.join(result_dir_path, filename), self._env.get_ram()[RAM_start:RAM_end])
                state = env.em.get_state()
                state_path = os.path.join(ex_dir_state_path, f"RM_gameplay_{steps}.state")
                with open(state_path, 'wb') as f:
                    pickle.dump(state, f,  pickle.HIGHEST_PROTOCOL)
            steps += 1
            in_state_frames += 1
            if(not norender):
                env.render()

def main():
    ex_dir_path = f"GameplayData/{retro_name}/{experiment_name}"
    states = list()

    states_urls = os.listdir(ex_dir_path + f"/HumanGameplay/States")
    for f in states_urls:
        states.append(np.load(os.path.join(ex_dir_path + f"/HumanGameplay/States",f), allow_pickle=True))

    states = np.array(states)
    print("Found " + str(len(states)) + " states under " + ex_dir_path + f"/HumanGameplay/States")
    if(len(states) == 0):
        raise ValueError("Number of states to run RM must be greater than 0")
        exit()

    env = retro.make(game=retro_name, use_restricted_actions=retro.Actions.ALL)

    if(algorithm == "Random"):
        # No dist currently for random
        random_play(env, states, ex_dir_path, False)

if __name__ == '__main__':
    main()
