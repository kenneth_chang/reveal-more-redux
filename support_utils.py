import pickle, os, time
import numpy as np
import shutil

import shutil
import retro
import glob

def human_experiment_create_folders(game_name:str, ex_name:str, clobber:bool)->str:
    global experiments_dir_path

    gameplay_data_dir = f"GameplayData"
    experiments_dir_path = f"{gameplay_data_dir}/{game_name}/{ex_name}/"
    utils_dir = f"{gameplay_data_dir}/Utils/"

    if os.path.exists(experiments_dir_path) and clobber:
        print("WARNING: Clobber is set to ON, overwriting old experiment data")
        shutil.rmtree(experiments_dir_path)

    if not os.path.exists(experiments_dir_path):
        print("Experiments dir does not exist, making Experiments dir at " + experiments_dir_path)
        os.makedirs(experiments_dir_path) # Top level experiments
        os.makedirs(experiments_dir_path + "KeyDistrbutions/")
        os.makedirs(experiments_dir_path + "HumanGameplay/Screenshots/")
        os.makedirs(experiments_dir_path + "HumanGameplay/RAM/")
        os.makedirs(experiments_dir_path + "HumanGameplay/States/")
        os.makedirs(experiments_dir_path + "HumanGameplay/Buttons/")
        os.makedirs(experiments_dir_path + "RMGameplay/Screenshots/")
        os.makedirs(experiments_dir_path + "RMGameplay/RAM/")
        os.makedirs(experiments_dir_path + "RMGameplay/States/")
        os.makedirs(experiments_dir_path + "RMGameplay/Buttons/")
        os.makedirs(experiments_dir_path + "BK2Analysis/Screenshots/")
        os.makedirs(experiments_dir_path + "BK2Analysis/RAM/")
        os.makedirs(experiments_dir_path + "BK2Analysis/States/")
        os.makedirs(experiments_dir_path + "BK2Analysis/Buttons/")

    os.makedirs(utils_dir + "ConsoleButtonMappings", exist_ok=True)
    return experiments_dir_path, utils_dir
